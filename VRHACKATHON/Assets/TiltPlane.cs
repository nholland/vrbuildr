﻿using UnityEngine;
using System.Collections;

public class TiltPlane : MonoBehaviour {

    public float s = 0.9f;
    public float maxtilt=45;
    float OX = 0;
    float OY = 0;
    float OZ = 0;

    float oldX;
    float oldY;
    float oldz;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float oldX = OX;
        float oldY = OY;
        float oldz = OZ;
        OX = UDPReceive.lastRecievedUPDVector.x;
        OY = UDPReceive.lastRecievedUPDVector.y;
        OZ = UDPReceive.lastRecievedUPDVector.z;

        OX = OX / (180 / maxtilt);
        OY = OY / (180 / maxtilt);
        OZ = OZ / (180 / maxtilt);
                //smooth
                OX = OX * s + oldX * (1 - s);
        OY = OY * s + oldY * (1 - s);
        OZ = OZ * s + oldY * (1 - s);

        gameObject.transform.eulerAngles = new Vector3(OX, OY, OZ);
        
        
	}
}
