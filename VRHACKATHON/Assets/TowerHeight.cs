﻿using UnityEngine;
using System.Collections;

public class TowerHeight : MonoBehaviour {
    public GameObject tower;
    public static float maxHeight=0;
    float curheight = 0;

    // Use this for initialization
    void Start()
    {
        GetHeight();
    }
	
	// Update is called once per frame
	void Update () {
        GetHeight();
    }

    void GetHeight()
    {
        foreach (Transform child in transform)
        {
            curheight = child.gameObject.transform.localPosition.z;
            if (curheight > maxHeight)
            {
                maxHeight = curheight;
            }

        }
    }
}
