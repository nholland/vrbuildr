﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class timer : MonoBehaviour {
    public float timeLeft;
    public 
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        timeLeft -= Time.deltaTime;
        string minutes = Mathf.Floor(timeLeft / 60).ToString("00");
        string seconds = (timeLeft % 60).ToString("00");
       
               gameObject.GetComponent<Text>().text = minutes + ":"+seconds;
        if (timeLeft < 0)
        {
            gameOver.gameover = true;
        }
    }

    
}
