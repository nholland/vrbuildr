﻿using UnityEngine;
using System.Collections;
using VRTK;

public class lasersystem : MonoBehaviour {
    public GameObject Controler;
	// Use this for initialization
	void Start () {
        gameObject.GetComponent<ParticleSystem>().Stop();
        gameObject.GetComponent<AudioSource>().volume = 0;
    }
    bool firstpress = true;
	// Update is called once per frame
	void Update () {
        if (Controler.GetComponent<VRTK_ControllerEvents>().grabPressed)
        {
            gameObject.GetComponent<ParticleSystem>().Play();
            //gameObject.GetComponent<AudioSource>().Play();
            if (firstpress)
            {
                gameObject.GetComponent<AudioSource>().volume=1;
                //gameObject.GetComponent<AudioSource>().Play();
                firstpress = false;
            }
        }
        else
        {
            gameObject.GetComponent<ParticleSystem>().Stop();
            //gameObject.GetComponent<AudioSource>().Stop();
            gameObject.GetComponent<AudioSource>().volume = 0;
            firstpress = true;
        }
    }
}
