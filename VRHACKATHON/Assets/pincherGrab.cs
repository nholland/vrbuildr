﻿using UnityEngine;
using System.Collections;

public class pincherGrab : MonoBehaviour {

    Collider P;

    public Collider GetP()
    {
        return P;
    }
    void OnTriggerStay(Collider other)
    {
        if (other.tag == "block")
        {
            P = other;
            print("P entered"+other.name);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if(other.tag == "block")
        {
            P = null;
            print("P Exited"+other.name);
        }
    }
}
