﻿using UnityEngine;
using System.Collections;
using VRTK;

public class clampgrabber : MonoBehaviour {
    public GameObject Pincher1;
    public GameObject Pincher2;
    public GameObject clamp;
    Collider block;
    public bool grabbing;


    void start()
    {
        
       
    }
    
        
    public bool isGrabbing()
    {
        return grabbing;
    }
    void OnTriggerStay(Collider other)
    {
        if (other.tag == "block" && gameObject.GetComponentsInParent<VRTK_ControllerEvents>()[0].grabPressed)
        {
            
            other.transform.parent = gameObject.transform;
            other.attachedRigidbody.useGravity = false;
            other.attachedRigidbody.angularVelocity = new Vector3(0f, 0f, 0f);
            other.attachedRigidbody.velocity = new Vector3(0f, 0f, 0f);
            //other.attachedRigidbody.rotation = GetComponent<Transform>().rotation;
            other.gameObject.layer = 10;
            grabbing = true;
            block = other;
        }
        else if (grabbing==true)//(block!=null)
        {
             block.transform.parent = null;
             block.attachedRigidbody.useGravity = true;
            block.attachedRigidbody.angularVelocity = gameObject.GetComponentsInParent<VRTK_ControllerEvents>()[0].GetAngularVelocity();
            print("AV="+gameObject.GetComponentsInParent<VRTK_ControllerEvents>()[0].GetAngularVelocity());
            block.attachedRigidbody.velocity = gameObject.GetComponentsInParent<VRTK_ControllerEvents>()[0].GetVelocity();
            grabbing = false;
        }

    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "block"&grabbing==true)
        {
            other.transform.parent = null;
            other.attachedRigidbody.useGravity = true;
            block.attachedRigidbody.angularVelocity = gameObject.GetComponentsInParent<VRTK_ControllerEvents>()[0].GetAngularVelocity();
            block.attachedRigidbody.velocity = gameObject.GetComponentsInParent<VRTK_ControllerEvents>()[0].GetVelocity();

        }
    }
    

}
