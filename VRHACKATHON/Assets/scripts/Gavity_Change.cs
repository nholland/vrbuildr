﻿using UnityEngine;
using System.Collections;

public class Gavity_Change : MonoBehaviour {
    public float rotationDegreesPerSecond = 15f;
    public float rotationDegreesAmount;
    private float totalRotation;
    public float gravity = -5.0f;
    // Use this for initialization
    void Start () {
	    totalRotation =0;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(2))
            rotationDegreesAmount = 90;

        if (Mathf.Abs(totalRotation) < Mathf.Abs(rotationDegreesAmount))
            set_gravity();
    }
    void set_gravity()
    {
        float currentAngle = transform.rotation.eulerAngles.z;
        transform.rotation = Quaternion.AngleAxis(currentAngle + (Time.deltaTime * rotationDegreesPerSecond), Vector3.forward);
        totalRotation += Time.deltaTime * rotationDegreesPerSecond;
        Physics.gravity = transform.up*gravity;
    }
    
    
}
