﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Follow_Mouse : MonoBehaviour {
    public Collider otherCollider;
    public Transform Cube;
    public Transform Cube_2;
    public Transform Cube_3;
    Vector3 wantedPos;
    public List<GameObject> Blocks = new List<GameObject>();
    float xSize;
    public int startNum;
    // Use this for initialization
    void Start () {
        wantedPos = transform.position;
        int rot = 0;
        xSize = Cube.localScale.x;
        for (int i= 0; i < startNum; i++)
        {
           Create_Box(wantedPos, rot);
            if (rot == 0)
                rot = 90;
            else
                rot = 0;
            wantedPos.y += Cube.localScale.y;
        }
        
	}
    void OnTriggerEnter(Collider other){
        
    }
    // Update is called once per frame
    void Update () {

    }
    public void Create_Box(Vector3 pos, float rotAngle)
    {
        Transform T;
        
        Transform temp;
        
        temp = NextRandomBlock();
        if (rotAngle == 90)
        {
            T = Instantiate(temp, new Vector3(pos.x, pos.y, pos.z + xSize), Quaternion.Euler(0.0f, rotAngle, 0.0f)) as Transform;
            Blocks.Add(T.gameObject);
            temp = NextRandomBlock();
            T = Instantiate(temp, new Vector3(pos.x, pos.y, pos.z), Quaternion.Euler(0.0f, rotAngle, 0.0f)) as Transform;
            Blocks.Add(T.gameObject);
            temp = NextRandomBlock();
            T = Instantiate(temp, new Vector3(pos.x, pos.y, pos.z - xSize), Quaternion.Euler(0.0f, rotAngle, 0.0f)) as Transform;
            Blocks.Add(T.gameObject);
        }
        else
        {
            T = Instantiate(temp, new Vector3(pos.x + xSize, pos.y, pos.z), Quaternion.Euler(0.0f, rotAngle, 0.0f)) as Transform;
            Blocks.Add(T.gameObject);
            temp = NextRandomBlock();
            T = Instantiate(temp, new Vector3(pos.x, pos.y, pos.z), Quaternion.Euler(0.0f, rotAngle, 0.0f)) as Transform;
            Blocks.Add(T.gameObject);
            temp = NextRandomBlock();
            T = Instantiate(temp, new Vector3(pos.x - xSize, pos.y, pos.z), Quaternion.Euler(0.0f, rotAngle, 0.0f)) as Transform;
            Blocks.Add(T.gameObject);
        }

    }

    private Transform NextRandomBlock()
    {
        int x = UnityEngine.Random.Range(0, 3);
        Transform temp;
        switch ((int)x)
        {
            case 1:
                temp = Cube; break;
            case 2:
                temp = Cube_2; break;
            default:
                temp = Cube_3; break;
                 }
        return temp;
    }
}
