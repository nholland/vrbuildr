﻿
/*
 
    -----------------------
    UDP-Receive (send to)
    -----------------------
    // [url]http://msdn.microsoft.com/de-de/library/bb979228.aspx#ID0E3BAC[/url]
   
   
    // > receive
    // 127.0.0.1 : 8051
   
    // send
    // nc -u 127.0.0.1 8051
 
*/
using UnityEngine;
using System.Collections;

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;


public class UDPReceive : MonoBehaviour
{
    
    // receiving Thread
    Thread receiveThread;

    // udpclient object
    UdpClient client;

    // public
    // public string IP = "127.0.0.1"; default local
    public int port; // define > init
    public bool killer =true;
    // infos
    public static string lastReceivedUDPPacket;
    public static Vector3 lastRecievedUPDVector = new Vector3(0,0,0);
    public string allReceivedUDPPackets = ""; // clean up this from time to time!
    

    //// start from shell
    //private static void Main()
    //{
    //    UDPReceive receiveObj = new UDPReceive();
    //    receiveObj.init();

    //    string text = "";
    //    do
    //    {
    //        text = Console.ReadLine();
    //    }
    //    while (!text.Equals("exit"));
    //}

    // start from unity3d
    public void Start()
    {

        init();
    }
    //void Update()
    //{
    //    ReceiveData();
    //}
    // OnGUI
    void OnGUI()
    {
        Rect rectObj = new Rect(40, 10, 200, 400);
        GUIStyle style = new GUIStyle();
        style.alignment = TextAnchor.UpperLeft;
        GUI.Box(rectObj, "# UDPReceive\n127.0.0.1 " + port + " #\n"
                    + "shell> nc -u 127.0.0.1 : " + port + " \n"
                    + "\nLast Packet: \n" + lastReceivedUDPPacket
                    + "\n\nAll Messages: \n" + allReceivedUDPPackets
                , style);
    }

    // init
    private void init()
    {
        // Endpunkt definieren, von dem die Nachrichten gesendet werden.
        print("UDPSend.init()");

        // define port
        port = 5135;

        // status
        print("receiving to 127.0.0.1 : " + port);
        print("Test-receiving to this Port: nc -u 127.0.0.1  " + port + "");


        // ----------------------------
        // Abhören
        // ----------------------------
        // Lokalen Endpunkt definieren (wo Nachrichten empfangen werden).
        // Einen neuen Thread für den Empfang eingehender Nachrichten erstellen.
        receiveThread = new Thread(
            new ThreadStart(ReceiveData));
        receiveThread.IsBackground = true;
        receiveThread.Start();

    }
    

    // receive thread
    private void ReceiveData()
    {
        float[] testitem = new float[17];
        float accelX;
        float accelY;
        float accelZ;
        float GX;
        float GY;
        float GZ;
        float OX=0;
        float OY=0;
        float OZ=0;
        
        client = new UdpClient(port);
        while (killer)
        {

            try
            {
                // Bytes empfangen.
                IPEndPoint anyIP = new IPEndPoint(IPAddress.Any, 0);
                byte[] data = client.Receive(ref anyIP);
                
                for (int x = 0; x < data.Length; x += 4)
                {
                    flipset(ref data[x], ref data[x + 1], ref data[x + 2], ref data[x + 3]);
                }
                //byte temp = data[39];
                //data[39] = data[36];
                //data[36] = temp;
                //temp = data[38];
                //data[38] = data[37];
                //data[37] = temp;
                
                OX = BitConverter.ToSingle(data, 36);
                OY = BitConverter.ToSingle(data, 40);
                OZ = BitConverter.ToSingle(data, 44);

               
                //GX = BitConverter.ToSingle(data, 12);
                //GY = BitConverter.ToSingle(data, 16);
                //GZ = BitConverter.ToSingle(data, 20);

                //BinaryReader binRead = new BinaryReader(new MemoryStream(data));
                //accelX = binRead.ReadSingle();
                //accelY = binRead.ReadSingle();
                //accelZ = binRead.ReadSingle();
                //GX = binRead.ReadSingle();
                //GY = binRead.ReadSingle();
                //GZ = binRead.ReadSingle();
                //GX = binRead.ReadSingle();
                //GY = binRead.ReadSingle();
                //GZ = binRead.ReadSingle();
                //OX = binRead.ReadSingle();
                //OY = binRead.ReadSingle();
                //OZ = binRead.ReadSingle();

                // Bytes mit der UTF8-Kodierung in das Textformat kodieren.
                // string text = Encoding.UTF8.GetString(data);
                string text =  "    OX:" + OX.ToString() + "    OY:" + OY.ToString() + "    OZ:" + OZ.ToString();

                // Den abgerufenen Text anzeigen.
                // print(">> " + text);

                // latest UDPpacket
                lastReceivedUDPPacket = text;
                lastRecievedUPDVector = new Vector3( OY, 0, OZ);
                // ....
                //allReceivedUDPPackets = allReceivedUDPPackets + text;
                allReceivedUDPPackets = " ";

            }
            catch (Exception err)
            {
                print(err.ToString());
            }
        }
    }

    private void flipset(ref byte v1, ref byte v2, ref byte v3, ref byte v4)
    {
        byte temp = v4;
        v4 = v1;
        v1 = temp;
        temp = v3;
        v3 = v2;
        v2 = temp;
    }

    void OnDisable()
    {
        if (receiveThread != null)
            receiveThread.Abort();

        client.Close();
    }
    //void OnApplicationQuit()
    //{
    //    killer = false;
    //    receiveThread.Abort();
        

        
    //    //if (receiveThread.IsAlive)
    //    //{
    //    //    receiveThread.Abort();
    //    //}
        
    //    //receiver.Close();
    //}

    // getLatestUDPPacket
    // cleans up the rest
    //public string getLatestUDPPacket()
    //{
    //    allReceivedUDPPackets = "";
    //    return lastReceivedUDPPacket;
    //}
}