﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VRTK;

public class Trigger_Top : MonoBehaviour {
    List<GameObject> on_top = new List<GameObject>();
    bool changing;
    public int score;
    Vector2 direction;
    Vector2 side;
    Vector2 forwrd;
    // Use this for initialization
    void Start () {
        changing = false;
        score = 0;
        side = new Vector2(1, 0);
        forwrd = new Vector2(0, 1);
        direction = side;
    }
	
	// Update is called once per frame
	void Update () {
    }
    void LateUpdate()
    {
        if (changing)
        {
            Debug.Log(on_top.Count+" to be deleted");
            for (int i = 0; i < on_top.Count; i++)
                on_top[i].gameObject.layer = 0;

            on_top.Clear();
            score++;
            changing = false;
            if (direction == side)
                direction = forwrd;
            else
                direction = forwrd;
        }
    }
    
    void OnTriggerStay(Collider other)
    {
        Vector2 lookingAt = new Vector2(other.transform.forward.x, other.transform.forward.z);
            
        float DotProd = Vector2.Dot(direction, lookingAt);
        
      
        if (DotProd < 0.5 && DotProd > -0.5f && !on_top.Find(w => Equals(w.GetInstanceID(),other.gameObject.GetInstanceID())))
        {
            Debug.Log(" Dot: " + DotProd);
            Debug.Log(" Added");
            other.gameObject.layer = 0;
            on_top.Add(other.gameObject);
        }
          
        
        if (on_top.Count > 2)
        {
            GetComponentInParent<Collider_top>().Bring_top_up();
            changing = true;      
        }
    }
    
}
