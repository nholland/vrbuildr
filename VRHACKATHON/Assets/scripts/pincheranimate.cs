﻿using UnityEngine;
using System.Collections;
using VRTK;

public class pincheranimate : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (gameObject.GetComponentsInParent<VRTK_ControllerEvents>()[0].grabPressed)
        {
            gameObject.GetComponent<Animator>().SetBool("pinching", true);
            print("Started");
        }
        else
        {
            gameObject.GetComponent<Animator>().SetBool("pinching", false);
        }
    }
}
