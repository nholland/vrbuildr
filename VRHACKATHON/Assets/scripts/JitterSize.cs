﻿using UnityEngine;
using System.Collections;

public class JitterSize : MonoBehaviour {
    public float jitter;
	// Use this for initialization
	void Start () {
        Vector3 tran = transform.localScale;
        tran.x += Random.Range(-jitter, jitter);
        tran.y += Random.Range(-jitter, jitter);
        tran.z += Random.Range(-jitter, jitter);
        transform.localScale = tran;
    }
	
	
}
