﻿using UnityEngine;
using System.Collections;

public class Collider_top : MonoBehaviour {
    Vector3 final_pos;
    public GameObject Factory;
    int count;
    // Use this for initialization
    void Start () {
        count = Factory.GetComponent<Follow_Mouse>().Blocks.Count;
        for (int i = 0; i < count/3; i++)
            Bring_top_up();
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
    public void Bring_top_up()
    {
        final_pos = transform.position;
        final_pos.y += Factory.GetComponent<Follow_Mouse>().Blocks[0].transform.lossyScale.y/2;
        transform.position = final_pos;
        Debug.Log("new position = " + transform.position.y);
    }

}
